# Base image: https://hub.docker.com/_/golang/
FROM balenalib/rpi-raspbian:buster
MAINTAINER Vesa-Pekka Palmu <vpalmu@depili.fi>

# Install golint
ENV GO_VERSION=1.17.2
ENV GOPATH /go
ENV PATH ${GOPATH}/bin:$PATH:/usr/local/go/bin

ENV SDL2_VERSION=2.0.16
ENV SDL2_TTF_VERSION=2.0.15
ENV SDL2_GFX_VERSION=1.0.4
ENV SDL2_IMAGE_VERSION=2.0.5
ENV SDL2_MIXER_VERSION=2.0.4

ENV SDL2_TARBALL=SDL2-${SDL2_VERSION}.tar.gz
ENV TTF_TARBALL=SDL2_ttf-${SDL2_TTF_VERSION}.tar.gz
ENV GFX_TARBALL=SDL2_gfx-${SDL2_GFX_VERSION}.tar.gz
ENV IMAGE_TARBALL=SDL2_image-${SDL2_IMAGE_VERSION}.tar.gz
ENV MIXER_TARBALL=SDL2_mixer-${SDL2_MIXER_VERSION}.tar.gz

RUN apt-get update && apt-get install -y --no-install-recommends \
    wget \
    git \
    build-essential \
    libfontconfig-dev \
    qt5-default \
    automake \
    mercurial \
    libtool \
    libfreeimage-dev \
    libopenal-dev \
    libpango1.0-dev \
    libsndfile-dev \
    libudev-dev \
    libtiff5-dev \
    libwebp-dev \
    libasound2-dev \
    libaudio-dev \
    libxrandr-dev \
    libxcursor-dev \
    libxi-dev \
    libxinerama-dev \
    libxss-dev \
    libesd0-dev \
    freeglut3-dev \
    libmodplug-dev \
    libsmpeg-dev \
    libraspberrypi-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN wget -nv http://www.libsdl.org/release/${SDL2_TARBALL} &&\
    tar zxf ${SDL2_TARBALL} &&\
    rm -f ${SDL2_TARBALL} &&\
    cd SDL2-${SDL2_VERSION} &&\
    bash ./autogen.sh &&\
    ./configure \
    --prefix=/usr\
    --host=armv7l-raspberry-linux-gnueabihf \
    --disable-pulseaudio \
    --disable-esd \
    --disable-video-mir \
    --disable-video-wayland \
    --disable-video-x11 \
    --disable-video-opengl \
    --disable-video-vulkan \
    --disable-static &&\
    make && make install &&\
    cd .. && rm -fr SDL2-${SDL2_VERSION}

RUN wget -nv https://www.libsdl.org/projects/SDL_ttf/release/${TTF_TARBALL} &&\
    tar zxf ${TTF_TARBALL} &&\
    rm -f ${TTF_TARBALL} &&\
    cd SDL2_ttf-${SDL2_TTF_VERSION} &&\
    bash ./autogen.sh &&\
    ./configure &&\
    make && make install &&\
    cd .. && rm -fr SDL2_ttf-${SDL2_TTF_VERSION}

RUN wget -nv http://www.ferzkopp.net/Software/SDL2_gfx/${GFX_TARBALL} &&\
    tar zxf ${GFX_TARBALL} &&\
    rm -fr ${GFX_TARBALL} &&\
    cd SDL2_gfx-${SDL2_GFX_VERSION} &&\
    bash ./autogen.sh &&\
    ./configure --prefix=/usr --disable-mmx &&\
    make &&\
    make install &&\
    cd .. && rm -fr SDL2_gfx-${SDL2_GFX_VERSION}

RUN wget -nv https://www.libsdl.org/projects/SDL_image/release/${IMAGE_TARBALL} &&\
    tar zxf ${IMAGE_TARBALL} &&\
    rm -f ${IMAGE_TARBALL} &&\
    cd SDL2_image-${SDL2_IMAGE_VERSION} &&\
    bash ./autogen.sh &&\
    ./configure --prefix=/usr &&\
    make &&\
    make install &&\
    cd .. && rm -fr SDL2_image-${SDL2_IMAGE_VERSION}

RUN wget -nv https://www.libsdl.org/projects/SDL_mixer/release/${MIXER_TARBALL} &&\
    tar zxf ${MIXER_TARBALL} &&\
    rm -f ${MIXER_TARBALL} &&\
    cd SDL2_mixer-${SDL2_MIXER_VERSION} &&\
    bash ./autogen.sh &&\
    ./configure --prefix=/usr &&\
    make &&\
    make install &&\
    cd .. && rm -fr SDL2_mixer-${SDL2_MIXER_VERSION}

RUN wget -nv https://golang.org/dl/go${GO_VERSION}.linux-armv6l.tar.gz &&\
    tar -C /usr/local -xzf go${GO_VERSION}.linux-armv6l.tar.gz &&\
    rm -f go${GO_VERSION}.linux-armv6l.tar.gz

RUN go get -u golang.org/x/lint/golint


# Set Clang as default CC
ENV set_clang /etc/profile.d/set-clang-cc.sh
RUN echo "export CC=clang-7" | tee -a ${set_clang} && chmod a+x ${set_clang}
